// JavaScript code

// Object to store prices for each element
const elementPrices = {
    paper: 1.5,
    wood: 1.8,
    tissue: 2.0,
};

// Array to store selected elements and their prices
let elements = [];

// Function to add a selected element to the list
function addElement() {
    // Add selected element and its price to the list
    const element = document.getElementById('element').value;
    const price = elementPrices[element];
    elements.push({ element, price });
    // Update the displayed result
    updateResult();
    // Update the price input based on the selected element
    updatePrice();
}

// Function to delete the last added element from the list
function deleteElement() {
    // Delete the last added element
    elements.pop();
    // Update the displayed result
    updateResult();
    // Update the price input based on the selected element
    updatePrice();
}

// Function to update the price input based on the selected element
function updatePrice() {
    const selectedElement = document.getElementById('element').value;
    const priceInput = document.getElementById('price');
    // Update the price input value based on the selected element
    priceInput.value = elementPrices[selectedElement] || 0.5; // Default to 0.5 if the price is not found
}

// Function to calculate the total price based on selected size and elements
function calculatePrice() {
    // Get length and width in centimeters
    const lengthCm = parseFloat(document.getElementById('length').value);
    const widthCm = parseFloat(document.getElementById('width').value);

    // Convert length and width to square meters
    const areaM2 = (lengthCm / 100) * (widthCm / 100);

    // Display the calculated square meter
    document.getElementById('area').innerText = `Total Area: ${areaM2.toFixed(2)} m²`;

    // Get price per square meter
    const pricePerM2 = parseFloat(document.getElementById('price').value);

    // Calculate the total price
    const totalPrice = areaM2 * elements.reduce((sum, el) => sum + el.price, 0);

    // Display the total price
    document.getElementById('result').innerText = `Total Price: $${totalPrice.toFixed(2)}`;
    document.getElementById('client_credits').innerText = document.getElementById('name').value + ' ' + document.getElementById('surname').value;
}

// Function to update the displayed result with the selected elements
function updateResult() {
    // Get the result element
    const resultElement = document.getElementById('result');
    // Display the selected elements and their prices
    resultElement.innerText = `Selected Elements: ${elements.map(el => `${el.element} - $${el.price.toFixed(2)}`).join(', ')}`;
    // Show the custom size input section
    document.getElementById('custom-size-section').style.display = 'block';
}
