// function calculateTotal() {
//     const selectedFormat = document.getElementById('format').value;
//     const numExamples = parseInt(document.getElementById('num-examples').value);
//     const selectedMaterial = document.getElementById('material').value;
//
//     // Define the example rule
//     const exampleRule = {
//         A3: 1,
//         A4: 2,
//         A5: 4,
//         A6: 8,
//         A7: 16,
//         // Add other paper size rules as needed
//     };
//
//     if (!exampleRule[selectedFormat]) {
//         alert("Please select a valid paper format.");
//         return;
//     }
//
//     // Define the price per unit for each material
//     const pricePerUnit = {
//         paper: 1.5,
//         wood: 1.8,
//         tissue: 2.0,
//         // Add other material prices as needed
//     };
//
//     if (!pricePerUnit[selectedMaterial]) {
//         alert("Please select a valid material.");
//         return;
//     }
//
//     // Calculate the number of examples that will fit on the selected format
//     const numExamplesOnFormat = numExamples / exampleRule[selectedFormat];
//
//     // Calculate the total cost
//     const totalCost = numExamplesOnFormat * pricePerUnit[selectedMaterial];
//
//     // Display the result
//     document.getElementById('result').innerText = `Number of Examples on ${selectedFormat}: ${numExamplesOnFormat} | Total Cost: €${totalCost.toFixed(2)}`;
// }
// other version but with recto verso
function calculateTotal() {
    const selectedFormat = document.getElementById('format').value;
    const numExamples = parseInt(document.getElementById('num-examples').value);
    const selectedMaterial = document.getElementById('material').value;
    const isRectoVerso = document.getElementById('recto-verso').checked; // Use checked property for a checkbox
    const rectoVersoPrice = parseFloat(document.getElementById('recto-verso-price').value) || 0; // Get recto verso price

    // Define the example rule
    const exampleRule = {
        A3: 1,
        A4: 2,
        A5: 4,
        A6: 8,
        A7: 16,
        // Add other paper size rules as needed
    };

    if (!exampleRule[selectedFormat]) {
        alert("Please select a valid paper format.");
        return;
    }

    // Define the price per unit for each material
    const pricePerUnit = {
        paper: 1.5,
        wood: 1.8,
        tissue: 2.0,
        // Add other material prices as needed
    };

    if (!pricePerUnit[selectedMaterial]) {
        alert("Please select a valid material.");
        return;
    }

    // Calculate the number of examples that will fit on the selected format
    const numExamplesOnFormat = numExamples / exampleRule[selectedFormat];

    // Adjust for recto-verso
    const totalExamples = isRectoVerso ? numExamplesOnFormat * 2 : numExamplesOnFormat;

    // Calculate the total cost
    const totalCost = isRectoVerso ? numExamples * rectoVersoPrice : totalExamples * pricePerUnit[selectedMaterial];

    // Display the result
    document.getElementById('result').innerText = `Number of Examples on ${selectedFormat}: ${totalExamples} | Total Cost: €${totalCost.toFixed(2)}`;
}